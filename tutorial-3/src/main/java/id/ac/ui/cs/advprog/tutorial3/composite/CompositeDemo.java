package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;

public class CompositeDemo {
    private static void printEmployeesInfo(Employees employees) {
        System.out.println(employees.getName() + " :" + employees.getRole());
    }

    public static void main(String[] args) {
        Company company = new Company();

        Ceo ceo = new Ceo("The CEO", 1900000);
        company.addEmployee(ceo);

        Cto cto = new Cto("The CTO", 1900000);
        company.addEmployee(cto);

        BackendProgrammer backendProgrammer = new BackendProgrammer("The Backend", 1400000);
        company.addEmployee(backendProgrammer);

        System.out.println(company.getNetSalaries());

        System.out.println("Employees List:");
        company.getAllEmployees().stream().forEach(CompositeDemo::printEmployeesInfo);
    }
}
