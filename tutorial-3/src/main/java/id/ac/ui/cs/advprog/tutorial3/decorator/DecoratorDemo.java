package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorDemo {
    public static void main(String[] args) {
        Food food = null;
        food = BreadProducer.THIN_BUN.createBreadToBeFilled();
        food = FillingDecorator.CHICKEN_MEAT.addFillingToBread(food);
        food = FillingDecorator.TOMATO.addFillingToBread(food);

        System.out.println("Made \"" + food.getDescription() + "\" with cost: $" + food.cost());
    }
}
