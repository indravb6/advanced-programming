package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        ListIterator<Command> listIterator = commands.listIterator();
        while (listIterator.hasNext()) {
            listIterator.next().execute();
        }
    }

    @Override
    public void undo() {
        ListIterator<Command> listIterator = commands.listIterator(commands.size());
        while (listIterator.hasPrevious()) {
            listIterator.previous().undo();
        }
    }
}
