package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Test;

public class DepokPizzaStoreTest {
    @Test
    public void testCheesePizza() {
        PizzaStore dpStore = new DepokPizzaStore();

        Pizza pizza = dpStore.orderPizza("cheese");
        String expected = "---- Depok Style Cheese Pizza ----\n"
                + "ThickCrust style extra thick crust dough\nTomato sauce with plum tomatoes\nShredded Parmesan\n";
        assertEquals(expected, pizza.toString());
    }

    @Test
    public void testVeggiePizza() {
        PizzaStore dpStore = new DepokPizzaStore();

        Pizza pizza = dpStore.orderPizza("veggie");
        String expected = "---- Depok Style Veggie Pizza ----\n"
                + "ThickCrust style extra thick crust dough\nTomato sauce with plum tomatoes\nShredded Parmesan\n"
                + "Black Olives, Eggplant, Garlic, Mushrooms\n";
        assertEquals(expected, pizza.toString());
    }
}
