package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Test;

public class NewYorkPizzaStoreTest {
    @Test
    public void testCheesePizza() {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        String expected = "---- New York Style Cheese Pizza ----\n"
                + "Thin Crust Dough\nMarinara Sauce\nReggiano Cheese\n";
        assertEquals(expected, pizza.toString());
    }

    @Test
    public void testVeggiePizza() {
        PizzaStore dpStore = new NewYorkPizzaStore();

        Pizza pizza = dpStore.orderPizza("veggie");
        String expected = "---- New York Style Veggie Pizza ----\n"
                + "Thin Crust Dough\nMarinara Sauce\nReggiano Cheese\nGarlic, Onion, Mushrooms, Red Pepper\n";
        assertEquals(expected, pizza.toString());
    }
}
