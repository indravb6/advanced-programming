package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class DepokDough implements Dough {
    public String toString() {
        return "Depok Dough";
    }
}
