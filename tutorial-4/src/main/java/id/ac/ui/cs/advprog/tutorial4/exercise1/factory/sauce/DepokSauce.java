package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class DepokSauce implements Sauce {
    public String toString() {
        return "Depok Sauce";
    }
}
