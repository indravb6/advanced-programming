package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // create an object of SingleObject
    public static final Singleton instance = new Singleton();

    // Get the only object available
    public static Singleton getInstance() {
        return instance;
    }

    // make the constructor private so that this class cannot be
    // instantiated
    private Singleton() {
    }
}
